#ifndef PM_H
#define PM_H

#include <rte_ether.h>
#include <rte_ip.h>
#include <rte_udp.h>
#include <rte_tcp.h>
#include <rte_vxlan.h>
#include "headers.h"

struct packet_model
{
    struct
    {
        struct bench_v4_hdr bench;
    } bench_v4;

    struct
    {
        struct bench_v6_hdr bench;
    } bench_v6;

    int is_bench_v4;
    int is_bench_v6;
} __rte_packed;

#endif
