#ifndef NDN_H
#define NDN_H

#include <unistd.h>
#include <rte_ether.h>
#include "common.h"

struct bench_v6_hdr {
  /* standard headers */
  struct rte_ether_hdr ether;
  struct rte_ipv6_hdr ipv6;
  struct rte_udp_hdr udp;

  /* payload */
  char payload[0];
}  __rte_packed;

struct bench_v4_hdr {
  /* standard headers */
  struct rte_ether_hdr ether;
  struct rte_ipv4_hdr ipv4;
  struct rte_udp_hdr udp;

  /* payload */
  char payload[0];
}  __rte_packed;

#endif
