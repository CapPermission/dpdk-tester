#include "tload.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <rte_memory.h>
#include <netinet/in.h>
#include <assert.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "pm.h"
#include "common.h"

uint32_t pkt_length = 64;

static in_addr_t    src_ip_base = 0;
static in_addr_t    dst_ip_base = 0;
static uint16_t     src_port_base = 0;
static uint16_t     dst_port_base = 0;

void set_mac(struct rte_ether_hdr *eth_hdr, char* d_mac_str, char* s_mac_str, uint16_t proto){
    int i = 0;
    uint8_t *mac_p = NULL;
    static uint8_t base = 0;
    
    memset(&(eth_hdr->d_addr), 0, sizeof(eth_hdr->d_addr));
    memset(&(eth_hdr->s_addr), 0, sizeof(eth_hdr->s_addr));

    /* s mac */
    if(!s_mac_str){
        for(i = 0; i < 6; ++i){
            eth_hdr->s_addr.addr_bytes[i] = (uint8_t)(rand() % 255);
        }
    }
    else{
        mac_p = &(eth_hdr->s_addr.addr_bytes[0]);
        sscanf(s_mac_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",      
                                                mac_p, mac_p + 1, mac_p + 2,
                                                mac_p + 3, mac_p + 4, mac_p + 5);
    }

    /* d mac */
    if(!d_mac_str){
        for(i = 0; i < 6; ++i){
            eth_hdr->d_addr.addr_bytes[i] = (uint8_t)(rand() % 255);
        }
    }
    else{
        mac_p = &(eth_hdr->d_addr.addr_bytes[0]);
        sscanf(d_mac_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",      
                                                mac_p, mac_p + 1, mac_p + 2,
                                                mac_p + 3, mac_p + 4, mac_p + 5);
    }

    eth_hdr->ether_type = htons(proto);
}

void set_ipv4(struct rte_ipv4_hdr *ipv4, char * s_ip_str, char * d_ip_str){
    //ipv4 header
    ipv4->next_proto_id = 17;
    ipv4->version_ihl = (uint8_t)0x45;
    ipv4->type_of_service = (uint8_t)0;
    ipv4->total_length = htons((uint16_t)(pkt_length - 18));
    ipv4->packet_id = 0;
    ipv4->fragment_offset = 0x0040;//DF
    ipv4->time_to_live = 0xff;
    ipv4->hdr_checksum = 0;
    
    /* s ip */
    if(!s_ip_str){
        ipv4->src_addr = (uint32_t)rand();
    }
    else{
        ipv4->src_addr = rte_cpu_to_be_32((uint32_t)strtoul(s_ip_str, NULL, 16));
    }
    
    /* d ip */
    if(!d_ip_str){
        ipv4->dst_addr = (uint32_t)rand();
    }
    else{
        ipv4->dst_addr = rte_cpu_to_be_32((uint32_t)strtoul(d_ip_str, NULL, 16));
    }
    
    ipv4->hdr_checksum = rte_ipv4_cksum(ipv4);   
}

void set_ipv6(struct rte_ipv6_hdr *ipv6, uint8_t * s_ip_str, uint8_t * d_ip_str){
    //ipv6 header
    ipv6->vtc_flow = 0;
    ipv6->payload_len = htons((uint16_t)(pkt_length - 18));
    ipv6->proto = 17;
    ipv6->hop_limits = 0;
    
    /* s ip */
    if(!s_ip_str){
        ipv6->src_addr[0]  = (uint8_t)rand() % 256;
        ipv6->src_addr[1]  = (uint8_t)rand() % 256;
        ipv6->src_addr[2]  = (uint8_t)rand() % 256;
        ipv6->src_addr[3]  = (uint8_t)rand() % 256;
        ipv6->src_addr[4]  = (uint8_t)rand() % 256;
        ipv6->src_addr[5]  = (uint8_t)rand() % 256;
        ipv6->src_addr[6]  = (uint8_t)rand() % 256;
        ipv6->src_addr[7]  = (uint8_t)rand() % 256;
        ipv6->src_addr[8]  = (uint8_t)rand() % 256;
        ipv6->src_addr[9]  = (uint8_t)rand() % 256;
        ipv6->src_addr[10] = (uint8_t)rand() % 256;
        ipv6->src_addr[11] = (uint8_t)rand() % 256;
        ipv6->src_addr[12] = (uint8_t)rand() % 256;
        ipv6->src_addr[13] = (uint8_t)rand() % 256;
        ipv6->src_addr[14] = (uint8_t)rand() % 256;
        ipv6->src_addr[15] = (uint8_t)rand() % 256;
        ipv6->src_addr[16] = (uint8_t)rand() % 256;
        ipv6->src_addr[17] = (uint8_t)rand() % 256;
    }
    else{
        memcpy(ipv6->src_addr, s_ip_str, 16);
    }
    
    /* d ip */
    if(!d_ip_str){
        ipv6->dst_addr[0]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[1]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[2]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[3]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[4]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[5]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[6]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[7]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[8]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[9]  = (uint8_t)rand() % 256;
        ipv6->dst_addr[10] = (uint8_t)rand() % 256;
        ipv6->dst_addr[11] = (uint8_t)rand() % 256;
        ipv6->dst_addr[12] = (uint8_t)rand() % 256;
        ipv6->dst_addr[13] = (uint8_t)rand() % 256;
        ipv6->dst_addr[14] = (uint8_t)rand() % 256;
        ipv6->dst_addr[15] = (uint8_t)rand() % 256;
        ipv6->dst_addr[16] = (uint8_t)rand() % 256;
        ipv6->dst_addr[17] = (uint8_t)rand() % 256;
    }
    else{
        memcpy(ipv6->dst_addr, d_ip_str, 16);
    }
}

void set_udp(struct rte_udp_hdr *udp, char *s_port_p, char *d_port_p){
    static int cnt = 0;

    /* s port*/
    if(!s_port_p){
        udp->src_port = (uint16_t)rand();
    }
    else{
        udp->src_port = rte_cpu_to_be_16((uint16_t)atoi(s_port_p));
    }

    /* d port */
    if(!d_port_p){
        udp->dst_port = (uint16_t)rand();
    }
    else{
        udp->dst_port = rte_cpu_to_be_16((uint16_t)atoi(d_port_p));
    }

    udp->dgram_cksum = 0;
    udp->dgram_len = htons((uint16_t)(pkt_length - 18 - 20));

    // DUMP_PORT(*udp);
}

int load_bench_trace_line(FILE *fp, struct packet_model *pm, int ip_proto)
{
    int i = 0;
    char buff[256] = "";
    char __attribute__((unused)) *tok[NB_FIELD];
    char *s = NULL;
    char *sp = NULL;

    /* v6 */
    uint8_t ipv6[16] = {0};
    uint64_t ipv6_seg[2];
    uint8_t* ipv6_char = NULL;

    if(fp){
      if(fgets(buff, 256, fp) == NULL)
      {
          return END_LINE;
      }
      for(i = 0, s = buff; i < NB_FIELD; i++, s = NULL)
      {
          tok[i] = strtok_r(s, ". \t\n", &sp);
          if(tok[i] == NULL) {
            break;
          }
      }
    }
    
    /* ipv6 */
    if(ip_proto == 6) {
        /* ether */
        set_mac(&pm->bench_v6.bench.ether, NULL, NULL, 0x86dd);

        /* IPv6 */
        ipv6_seg[1] = strtoull(tok[0], NULL, 16);
        ipv6_seg[0] = strtoull(tok[1], NULL, 16);
        ipv6_char = (uint8_t*)ipv6_seg;
        for(int i = 0; i < 16; ++i) {
            ipv6[15 - i] = ipv6_char[i];
        }
        set_ipv6(&pm->bench_v6.bench.ipv6, NULL, ipv6);

        /* udp */
        set_udp(&pm->bench_v6.bench.udp, NULL, NULL);

        pm->is_bench_v6 = 1; 
        pm->is_bench_v4 = 0;
    }
    /* ipv4 */
    else if(ip_proto == 4) {
        /* ether */
        set_mac(&pm->bench_v4.bench.ether, NULL, NULL, 0x0800);
        
        /* IPv4 */
        set_ipv4(&pm->bench_v4.bench.ipv4, NULL, tok[0]);

        /* udp */
        set_udp(&pm->bench_v4.bench.udp, NULL, NULL);

        pm->is_bench_v6 = 0; 
        pm->is_bench_v4 = 1;
    }
    
    return VALID_LINE;
}

int load_bench_trace(const char *file, struct packet_model pms[], int max_traces, int ip_proto)
{
    FILE *fp = NULL;
    int ret = 0;
    int count = 0;

    srand(time(NULL));
    
    if(file){
      fp = fopen(file, "rb");
      if(fp == NULL)
      {
          rte_exit(-1, "open trace file failure!\n");
      }
      while((ret = load_bench_trace_line(fp, &pms[count], ip_proto)) != END_LINE)
      {
          if(ret == VALID_LINE)
          {
              count++;
          }

          if(count == max_traces) {
            break;
          }
      }
    }

    printf("total trace %d, pkt_length=%u\n", count, pkt_length);

    if(fp) {
        fclose(fp);
    }

    return count;
}
