#define _GNU_SOURCE
#include <stdio.h>
#include <netinet/in.h>
#include <stddef.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>
#include <sched.h>//sched_getcpu()
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dlfcn.h>

#include <rte_common.h>
#include <rte_cycles.h>
#include <rte_memory.h>
#include <rte_mempool.h>
#include <rte_timer.h>
#include <rte_eal.h>
#include <rte_debug.h>
#include <rte_launch.h>
#include <rte_lcore.h>
#include <rte_per_lcore.h>
#include <rte_ethdev.h>

#include <rte_ether.h>
#include <rte_ip.h>
#include <rte_udp.h>
#include <rte_tcp.h>

#include "pm.h"
#include "tload.h"
#include "tx_mp.h"
#include "headers.h"

// #define NB_MBUF 65535
#define NB_MBUF ((1 << 18) - 1)
#define MBUF_CACHE_SIZE 512
#define NB_BURST 32//debug , original value is 32
#define NB_TXQ 32
#define NB_RXQ 32
#define NB_MAX_PORTS 64
#define MAX_CORES 256

#define NB_RXD_DFT 128//debug, original value is 128
#define NB_TXD_DFT 128//debug, original value is 512
#define NB_TEST 100000
const int nb_rxd = NB_RXD_DFT;
const int nb_txd = NB_TXD_DFT;

#define NB_MAX_PM 2000001  

#define TX_RATE_DFT 10000

#define PRINT_GAP 2

#define AUTO_TX_TUNING 0

/* tester mode: 0=v4, 1=v6 */
int ip_proto = 4;

/* lookup apt */
typedef void (*init_algo_unit)(const char*,double);
typedef int (*lookupUint)(uint32_t);
void* hash_trie_dlib_handle = NULL;
init_algo_unit init_algo_unit_api;
lookupUint lookup_api;

/* global data */
struct global_info
{
  uint32_t total_trace;
  char trace_file[256];
  uint64_t Mbps;
};

struct global_info ginfo =
{
  0,
  "",
  TX_RATE_DFT,
};

/* generate mbuf */
struct packet_model pms[NB_MAX_PM];

/* pkt length*/
extern uint32_t pkt_length;

/* tot txq_nb*/
int tot_txq_nb = 0;

/* bitmap of cores */
uint64_t t_ports_map = 0x1;
uint64_t fwd_ports_map = 0x1;

struct port_config{
  int port_id;
  int rxq_nb;
  int txq_nb;
} tot_port_conf[NB_MAX_PORTS];

static struct rte_eth_conf port_conf = {
    .rxmode = {
        .mq_mode = ETH_MQ_RX_RSS,
        .max_rx_pkt_len = RTE_ETHER_MAX_LEN,
        .split_hdr_size = 0,
        .offloads = DEV_RX_OFFLOAD_CHECKSUM,
    },
    .rx_adv_conf = {
        .rss_conf = {
            .rss_key = NULL,
            .rss_hf = ETH_RSS_IP,
        },
    },
    .txmode = {
        .mq_mode = ETH_MQ_TX_NONE,
    },
};

/* lcore main */
struct port_stats_info
{
  struct
  {
    uint64_t tx_total_pkts;
    uint64_t tx_last_total_pkts;
    uint64_t tx_total_bytes;
    uint64_t tx_last_total_bytes;
    uint64_t last_batch;
    uint64_t last_batch_bytes;
  }txq_stats[NB_TXQ];

  struct
  {
    uint64_t rx_total_pkts;
    uint64_t rx_last_total_pkts;
    uint64_t rx_total_bytes;
    uint64_t rx_last_total_bytes;
    uint64_t last_batch;
    uint64_t last_batch_bytes;
  }rxq_stats[NB_RXQ];

  uint64_t tx_total;
  uint64_t tx_pps;
  uint64_t tx_mbps;
  uint64_t rx_total;
  uint64_t rx_pps;
  uint64_t rx_mbps;
}port_stats[RTE_MAX_ETHPORTS];

struct lcore_args
{
  uint32_t port_id;
  uint8_t is_fwd_lcore;
  struct
  {
    struct rte_mbuf *m_table[NB_BURST] __rte_cache_aligned;
    struct rte_mempool *mp;
    uint32_t queue_id;
    struct rte_timer tim;
    uint8_t is_tx_lcore;
  }tx;
  struct
  {
    uint8_t is_rx_lcore;
    uint32_t queue_id;
  }rx;
  uint64_t speed;
  uint32_t trace_idx;
  
  /* speed switch */
  volatile bool speed_flag;
};

struct lcore_args lc_args[RTE_MAX_LCORE];

static void usage()
{
  printf("Usage: pkt-sender <EAL options> -- -t <trace_file> -s <Mbps> -L <pkt_length> -T <send_rounds>\n");
  rte_exit(-1, "invalid arguments!\n");
}

static void parse_params(int argc, char **argv)
{
  char opt;
  int accept = 0;
  char res_path[256]="";

  while((opt = getopt(argc, argv, "t:s:L:P:p:F:")) != -1)
  {
    switch(opt)
    {
      case 't': 
            printf("--->DPDK-TESTER-DBG:Trace file=%s\n", optarg);
                        rte_memcpy(ginfo.trace_file, optarg, strlen(optarg)+1); 
              accept = 1; 
                        break;
      case 's': 
            ginfo.Mbps = atoi(optarg);
            if(ginfo.Mbps <= 0)
            {
              rte_exit(EINVAL, "tx rate (Mbps) is invalid!\n");
            }

            printf("--->DPDK-TESTER-DBG:Send rate set to %lluMbps\n", (unsigned long long)ginfo.Mbps);

            break;
      case 'L':
            pkt_length = (uint32_t)atoi(optarg);
            printf("--->DPDK-TESTER-DBG:Pkt size is %uB, low bound is %uB\n", pkt_length, sizeof(struct bench_v6_hdr));
            if(pkt_length < sizeof(struct bench_v4_hdr) || pkt_length < sizeof(struct bench_v6_hdr)) {
              printf("PktLen can not fit benchmark header, at least %u, got %u\n", sizeof(struct bench_v6_hdr), pkt_length);
              assert(0);
            }
            break;
      case 'P':
            /* bitmap of ports conducting transmit */
            t_ports_map = (uint64_t)strtol(optarg, NULL, 16);
            printf("--->DPDK-TESTER-DBG:Transmit core map is 0x%lx\n", t_ports_map);
            break;
      
      case 'p':
            /* ip proto */
            ip_proto = (int)atoi(optarg);
            printf("--->DPDK-TESTER-DBG:IP protocol is IPv%d\n", ip_proto);
            assert(ip_proto == 4 || ip_proto == 6);
      case 'F':
            /* bitmap of ports conducting transmit */
            fwd_ports_map = (uint64_t)strtol(optarg, NULL, 16);
            printf("--->DPDK-TESTER-DBG:FWD core map is 0x%lx\n", fwd_ports_map);
            break;
      default: 
            usage();
            break;
    }
  }
  if(!accept)
  {
    usage();
  }
}

static void send_pkt_rate (__rte_unused struct rte_timer *timer, void *arg)
{
  struct lcore_args *largs = (struct lcore_args*)arg;
  struct rte_mempool *mp;
  uint32_t port_id;
  uint32_t queue_id;
  uint32_t count = 0;
  int ret;

  mp = largs->tx.mp;
  port_id = largs->port_id;
  queue_id = largs->tx.queue_id;
    
  for(; count < NB_BURST; count++)
  {
    if(largs->trace_idx == ginfo.total_trace)
    {
      largs->trace_idx = 0;
    }
    largs->tx.m_table[count] = generate_mbuf(pms[largs->trace_idx++], mp, pkt_length);
  }

  ret = rte_eth_tx_burst(port_id, queue_id, largs->tx.m_table, NB_BURST);

  port_stats[port_id].txq_stats[queue_id].tx_total_pkts += ret;
  while(ret < NB_BURST)
  {
    rte_pktmbuf_free(largs->tx.m_table[ret++]);
  }
}

static uint64_t calc_period(uint64_t speed)
{
  return (uint64_t) ( ( (NB_BURST * (pkt_length + 20) * 8 * rte_get_tsc_hz()) / (double) speed) * tot_txq_nb);
}

static int worker_main(void *args)
{
  struct rte_mbuf *rx_table[NB_BURST];
  struct lcore_args *largs;
  uint8_t is_rx = 0;
  uint8_t is_tx = 0;
  uint8_t is_fwd = 0;
  uint16_t rxq_id = 0;
  uint16_t txq_id = 0;
  uint16_t ret = 0;
  uint64_t cur_batch_bytes = 0;
  uint64_t __attribute__((unused)) recv_tsc;

  // struct ndn_hdr * ndn_hdr;//for ndn test
  struct bench_hdr *hdr;//for ndn test
  struct rte_mbuf *m;
  double alpha = 0.85;
  uint64_t cur_hz = rte_get_tsc_hz();
  int cur_test_round = 0;
  uint32_t port_id = 0;
  int worker_id = 0;

  largs = (struct lcore_args*)args;

  is_rx = largs->rx.is_rx_lcore;
  is_tx = largs->tx.is_tx_lcore;
  is_fwd = largs->is_fwd_lcore;
  port_id = largs->port_id;
  rxq_id = (uint16_t)(largs->rx.queue_id);
  txq_id = (uint16_t)(largs->tx.queue_id);
  worker_id = sched_getcpu();
  
  printf("[CPU#%d is working, is_rx==%hhu, is_tx=%hhu, is_fwd=%hhu]\n", worker_id, is_rx, is_tx, is_fwd);
    
  if(is_tx)
  {
    printf("lcore#%u send packet from port %u - queue %u!\n", rte_lcore_id(), port_id, rxq_id);

    port_stats[port_id].txq_stats[rxq_id].tx_total_pkts = 0;
    port_stats[port_id].txq_stats[rxq_id].tx_last_total_pkts = 0;
    rte_timer_init(&largs->tx.tim);

    uint64_t period = calc_period(largs->speed);
    printf("period %lu\n", period);
    rte_timer_reset(&largs->tx.tim, period, PERIODICAL, rte_lcore_id(), send_pkt_rate, largs);
  }
  
  port_stats[port_id].rxq_stats[rxq_id].rx_total_pkts = 0;
  port_stats[port_id].rxq_stats[rxq_id].rx_last_total_pkts = 0;
  port_stats[port_id].rxq_stats[rxq_id].rx_total_bytes = 0;
  port_stats[port_id].rxq_stats[rxq_id].rx_last_total_bytes = 0;

  for(;;) {
    cur_batch_bytes = 0;
    if(is_rx) {
      ret = rte_eth_rx_burst(port_id, rxq_id, rx_table, NB_BURST);
      port_stats[port_id].rxq_stats[rxq_id].rx_total_pkts += ret;

      recv_tsc = rte_rdtsc();
    
      /* rx mode (enabled for all workers) */
      for(int i=0; i<ret; i++){
          m = rx_table[i];
          rte_prefetch0(rte_pktmbuf_mtod(m, void *));
      
          // /* fetch header */
          hdr = rte_pktmbuf_mtod(m, struct bench_hdr *);
          // DUMP_HDR(hdr, "Recv");

          // if(is_fwd) {
          //   uint32_t le_ip = rte_cpu_to_be_32(hdr->ipv4.dst_addr);
          //   hdr->fin_res = lookup_api(le_ip);
          //   // printf("IP=%u.%u.%u.%u (%08x), result=%d\n",  (le_ip >> 24) & 0xff, (le_ip >> 16) & 0xff,
          //   //                                               (le_ip >>  8) & 0xff, (le_ip      ) & 0xff,
          //   //                                               le_ip, fin_res);
          // }

          // cur_batch_bytes += (((hdr->ipv4.total_length & 0xff00) >> 8) + ((hdr->ipv4.total_length & 0x00ff) << 8) + 18) + 24;

          // cur_batch_bytes += hdr->ipv6.payload_len;
          cur_batch_bytes += pkt_length;
      }
      port_stats[port_id].rxq_stats[rxq_id].rx_total_bytes += cur_batch_bytes;

      /* fwd mode */
      if(is_fwd && ret > 0) {
        uint16_t to_snd = 0;

        to_snd = rte_eth_tx_burst((port_id + 1) % 2, txq_id, rx_table, ret);

        port_stats[(port_id + 1) % 2].txq_stats[txq_id].tx_total_pkts += to_snd;

        for(uint16_t i = to_snd; i < ret; ++i) {
          rte_pktmbuf_free(rx_table[i]);
        }

        port_stats[(port_id + 1) % 2].txq_stats[txq_id].tx_total_bytes += (cur_batch_bytes * to_snd / ret);
      } 

      /* pure rx mode */
      else {
        for(int i = 0; i < ret; ++i) {
            rte_pktmbuf_free(rx_table[i]);
        }
      }
    }

    /* conduct actively conduct tx */
    if(is_tx)
    {
        #if AUTO_TX_TUNING
        if(largs->speed_flag) {
            uint64_t period = calc_period(largs->speed);
            rte_timer_reset(&largs->tx.tim, period, PERIODICAL, rte_lcore_id(), send_pkt_rate, largs);
            largs->speed_flag = false;
        }
        #endif
        rte_timer_manage();
    }
  }

    return 0;
}

static void print_stats(int nb_ports)
{
  int i, j;

  uint64_t rx_total;
  uint64_t rx_last_total;
  uint64_t rx_bytes_total;
  uint64_t rx_last_bytes_total;

  uint64_t tx_total;
  uint64_t tx_last_total;
  uint64_t tx_bytes_total;
  uint64_t tx_last_bytes_total;

  uint64_t last_cyc, cur_cyc;
  uint64_t frame_len;
  int cur_rxq_nb = 0;
  int cur_txq_nb = 0;
  
  uint64_t  tot_tx_pps = 0;
  uint64_t  tot_tx_mbps = 0;
  uint64_t  tot_rx_pps = 0;
  uint64_t  tot_rx_mbps = 0;

  #if AUTO_TX_TUNING
  /* tune tx speed */
  double drop_rate = 0, pre_drop_rate = 0;
  double dup_cnt = 0;
  uint32_t core_id = 0;
  bool do_tuning = false;
  #endif

  /* framce size + premable(8) + crc(4) + framce gap(12)*/
  frame_len = pkt_length + 24;
  double time_diff;
  last_cyc = rte_get_tsc_cycles();
  for(;;)
  {
    sleep(PRINT_GAP);
    system("clear");
    tot_tx_pps = 0;
    tot_tx_mbps = 0;
    tot_rx_pps = 0;
    tot_rx_mbps = 0;

    for(i = 0; i < nb_ports; i++)
    {
      tx_total = tx_last_total = 0;
      rx_total = rx_last_total = 0;
      rx_bytes_total = rx_last_bytes_total = 0;
      cur_rxq_nb = tot_port_conf[i].rxq_nb;
      cur_txq_nb = tot_port_conf[i].txq_nb;

      /* txq */
      for(j = 0; j < cur_txq_nb; j++)
      {
        tx_total += port_stats[i].txq_stats[j].tx_total_pkts;
        tx_last_total += port_stats[i].txq_stats[j].tx_last_total_pkts;
        tx_bytes_total += port_stats[i].txq_stats[j].tx_total_bytes;
        tx_last_bytes_total += port_stats[i].txq_stats[j].tx_last_total_bytes;

        /* pkts */
        port_stats[i].txq_stats[j].last_batch = (port_stats[i].txq_stats[j].tx_total_pkts
                            - port_stats[i].txq_stats[j].tx_last_total_pkts);

        port_stats[i].txq_stats[j].tx_last_total_pkts = port_stats[i].txq_stats[j].tx_total_pkts;

        /* bytes */
        port_stats[i].txq_stats[j].last_batch_bytes = (port_stats[i].txq_stats[j].tx_total_bytes
                            - port_stats[i].txq_stats[j].tx_last_total_bytes);

        port_stats[i].txq_stats[j].tx_last_total_bytes = port_stats[i].txq_stats[j].tx_total_bytes;
      }

      /* rxq */
      for(j = 0; j < cur_rxq_nb; j++)
      {
        rx_total += port_stats[i].rxq_stats[j].rx_total_pkts;
        rx_last_total += port_stats[i].rxq_stats[j].rx_last_total_pkts;
        rx_bytes_total += port_stats[i].rxq_stats[j].rx_total_bytes;
        rx_last_bytes_total += port_stats[i].rxq_stats[j].rx_last_total_bytes;

        // pkts
        port_stats[i].rxq_stats[j].last_batch = (port_stats[i].rxq_stats[j].rx_total_pkts
                            - port_stats[i].rxq_stats[j].rx_last_total_pkts);

        port_stats[i].rxq_stats[j].rx_last_total_pkts = port_stats[i].rxq_stats[j].rx_total_pkts;

        // bytes
        port_stats[i].rxq_stats[j].last_batch_bytes = (port_stats[i].rxq_stats[j].rx_total_bytes
                            - port_stats[i].rxq_stats[j].rx_last_total_bytes);

        port_stats[i].rxq_stats[j].rx_last_total_bytes = port_stats[i].rxq_stats[j].rx_total_bytes;
      }
      cur_cyc = rte_get_tsc_cycles();
      time_diff = (cur_cyc - last_cyc) / (double)rte_get_tsc_hz();

      port_stats[i].tx_total = tx_total;
      port_stats[i].tx_pps = (uint64_t)((tx_total - tx_last_total) / time_diff);
      port_stats[i].tx_mbps = port_stats[i].tx_pps * (frame_len) * 8 / (1<<20);

      port_stats[i].rx_total = rx_total;
      port_stats[i].rx_pps = (uint64_t)((rx_total - rx_last_total) / time_diff);
      port_stats[i].rx_mbps = (((rx_bytes_total - rx_last_bytes_total) * 8) / (1<<20)) / time_diff;
    }

    unsigned long long _pps = 0;
    unsigned long long _mbps = 0;

    last_cyc = rte_get_tsc_cycles();
    printf("nb_ports is %d\n", nb_ports);
    printf("============================\n");
    for(i = 0; i < nb_ports; i++)
    {
      printf("Port %d Statistics:\n", i);
      printf("tx rate: %llu pps\n", (unsigned long long)port_stats[i].tx_pps);
      printf("tx rate: %llu Mbps\n", (unsigned long long)port_stats[i].tx_mbps);
      printf("       : %.2lf Gbps\n", (double)port_stats[i].tx_mbps / 1000);
      if( (1<<i) & t_ports_map) {
        for(j = 0; j < tot_port_conf[i].txq_nb; ++j){
          _pps =  (unsigned long long)(port_stats[i].txq_stats[j].last_batch / time_diff);
          _mbps = (unsigned long long)(_pps * frame_len * 8 / (1<<20));

          printf("--->txq#%2d: %10llu pps,\t%5llu mbps\n", j, _pps, _mbps );
          
          tot_tx_pps += _pps;
          tot_tx_mbps += _mbps; 
        }
      }

      else if( (1<<i) & fwd_ports_map ) {
        for(j = 0; j < tot_port_conf[i].txq_nb; ++j){
          _pps =  (unsigned long long)(port_stats[i].txq_stats[j].last_batch / time_diff);
          _mbps = ((unsigned long long)(port_stats[i].txq_stats[j].last_batch_bytes * 8) / (1<<20)) / time_diff;

          printf("--->txq#%2d: %10llu pps,\t%5llu mbps\n", j, _pps, _mbps );
          
          tot_tx_pps += _pps;
          tot_tx_mbps += _mbps; 
        }
      }
      printf("tx total: %llu\n", (unsigned long long)port_stats[i].tx_total);
      printf("\n");

      printf("rx rate: %llu pps\n", (unsigned long long)port_stats[i].rx_pps);
      printf("rx rate: %llu Mbps\n", (unsigned long long)port_stats[i].rx_mbps);
      printf("       : %.2lf Gbps\n", (double)port_stats[i].rx_mbps / 1000);
      for(j = 0; j < tot_port_conf[i].rxq_nb; ++j){
        /* statistic */
        _pps =  (unsigned long long)(port_stats[i].rxq_stats[j].last_batch / time_diff);
        _mbps = ((unsigned long long)(port_stats[i].rxq_stats[j].last_batch_bytes * 8) / (1<<20)) / time_diff;
        
        printf("--->rxq#%2d: %8llu pps, %6llu Mbps\n", j, _pps, _mbps);
        
        tot_rx_pps += _pps;
        tot_rx_mbps += _mbps;
      }

      printf("rx total: %llu\n", (unsigned long long)port_stats[i].rx_total);
      printf("============================\n");
    }
    
    printf("-------------->Total<----------------\n");
    printf("Tx: %.3lf Mpps, %lu Mbps\n", ((double)tot_tx_pps) / 1000000, tot_tx_mbps); 
    printf("Rx: %.3lf Mpps, %lu Mbps\n", ((double)tot_rx_pps) / 1000000, tot_rx_mbps);

    #if AUTO_TX_TUNING
    drop_rate = (double)(tot_tx_pps - tot_rx_pps) / (double)tot_tx_pps;

    if(drop_rate > 0.01 && pre_drop_rate > 0.01) {
        dup_cnt += 1;
    }
    pre_drop_rate = drop_rate;

    if(dup_cnt == 10) {
        printf("!!!!!!!!Speed Tuning!!!!!!!!!!\n");
        if(drop_rate > 0.01) {
            ginfo.Mbps = ginfo.Mbps * 99 / 100; 
        }

        /* reset */
        dup_cnt = 0;
        uint32_t i;
        RTE_LCORE_FOREACH_WORKER(i){
            lc_args[i].speed = ginfo.Mbps * 1000 * 1000;

            /* wait last tune */
            while(lc_args[i].speed_flag == true);

            /* switch */
            lc_args[i].speed_flag = true;
        }
    }
    #endif
  }
}

static int one_port_setup(struct rte_mempool *mp, int port_id, int core_for_port_list[], int tx_core_list[], int fwd_core_list[])
{
  int ret;
  int queue_id;
  int rxq_nb = 0;
  int txq_nb = 0;
        
  struct rte_eth_dev_info dev_info;
  struct rte_eth_txconf *tx_conf;
  struct rte_eth_rxconf *rx_conf;
  
  /* scan core_for_port and tx_core list */
  uint32_t i;
  RTE_LCORE_FOREACH_WORKER(i){
    if(core_for_port_list[i] == port_id){
      /* all cores are rx core */
      lc_args[i].port_id = port_id;
      lc_args[i].rx.is_rx_lcore = 1;
      lc_args[i].rx.queue_id = rxq_nb++;
      lc_args[i].tx.mp = mp;

      /* set tx core */
      if(tx_core_list[i] == 1){
        lc_args[i].tx.is_tx_lcore = 1;
        lc_args[i].tx.queue_id = txq_nb++;
        lc_args[i].speed = ginfo.Mbps * 1000 * 1000;
        lc_args[i].trace_idx = 0;
      }

      /* set fwd core */
      if(fwd_core_list[i] == 1){
        lc_args[i].is_fwd_lcore = 1;
        lc_args[i].tx.queue_id = txq_nb++;
        lc_args[i].speed = ginfo.Mbps * 1000 * 1000;
        lc_args[i].trace_idx = 0;
      }
    }
  }
  
  printf("- - -> Port#%u, rxq=%d, txq=%d\n", port_id, rxq_nb, txq_nb);
  
  /* configure txq_nb and rxq_nb of port*/
  ret = rte_eth_dev_configure(port_id, rxq_nb, txq_nb, &port_conf);
  if(ret < 0)
  {
    rte_exit(-1, "port %d configure failure!\n", port_id);
  }

  /* set txq queue */
  for(queue_id = 0; queue_id < txq_nb; queue_id++)
  {
    //modify tx thresh
    rte_eth_dev_info_get(port_id, &dev_info);                    
    tx_conf = &dev_info.default_txconf;

    ret = rte_eth_tx_queue_setup(port_id, queue_id, nb_txd, rte_eth_dev_socket_id(port_id), tx_conf);
    if(ret != 0)
    {
      rte_exit(-1, "port %d tx queue setup failure!\n", port_id);
    }
  }

  /* set rxq queue */
  for(queue_id = 0; queue_id < rxq_nb; queue_id++)
  {
    //modify rx thresh
    rte_eth_dev_info_get(port_id, &dev_info);
    rx_conf = &dev_info.default_rxconf;

    ret = rte_eth_rx_queue_setup(port_id, queue_id, nb_rxd, rte_eth_dev_socket_id(port_id), rx_conf, mp);
    if(ret != 0)
    {
      rte_exit(-1, "port %d rx queue setup failure!\n", port_id);
    }
  }

  /* update tot port conf */
  tot_port_conf[port_id].port_id = port_id;
  tot_port_conf[port_id].rxq_nb = rxq_nb;
  tot_port_conf[port_id].txq_nb = txq_nb;
  tot_txq_nb += txq_nb;
  
  ret = rte_eth_dev_start(port_id);
  
  if(ret < 0)
  {
    rte_exit(-1, "port %d start failure!\n", port_id);
  }
  
  printf("Set promiscuous enable for %d\n", port_id);
    rte_eth_promiscuous_enable(port_id);

    return ret;
}

void configure_core_port_map(int port_ids[], int is_tx_core[], int is_fwd_core[], int nb_cores, int nb_ports)
{
  const int cores_per_port = (int)((nb_cores - 1) / nb_ports);
  int cur_port = 0;
  int cores_count_one_port = 0;
  uint32_t lcore_id;

  printf("--->Cores=%d, Ports=%d, cores_per_port=%d\n", nb_cores, nb_ports, cores_per_port); 
  RTE_LCORE_FOREACH_WORKER(lcore_id){
    port_ids[lcore_id] = cur_port;
    printf("------>Core%u works for port#%d\n", lcore_id, port_ids[lcore_id]);
    cores_count_one_port += 1;
    if(cores_count_one_port == cores_per_port){
      cores_count_one_port = 0;
      cur_port += 1;
    }
  }

  printf("t_ports_map: 0x%lx, foward_ports_map=%u\n", t_ports_map, fwd_ports_map);
  RTE_LCORE_FOREACH_WORKER(lcore_id){
    /* setup cores of TX port */
    if( ((1UL<<port_ids[lcore_id]) & t_ports_map)){
      is_tx_core[lcore_id] = 1;
    }
    else{
      is_tx_core[lcore_id] = 0;
    }

    /* setup cores of FWD port */
    if( ((1UL<<port_ids[lcore_id]) & fwd_ports_map)){
      is_fwd_core[lcore_id] = 1;
    }
    else{
      is_fwd_core[lcore_id] = 0;
    }
  }
}

int main(int argc, char **argv)
{
  int ret;
  ret = rte_eal_init(argc, argv);
  if(ret < 0)
  {
    rte_exit(-1, "rte_eal_init failure!\n");
  }

  struct rte_mempool *mbuf_pool;
  mbuf_pool = rte_pktmbuf_pool_create("MBUF_POOL", NB_MBUF, MBUF_CACHE_SIZE, 0, RTE_MBUF_DEFAULT_BUF_SIZE, rte_socket_id());
  if(mbuf_pool == NULL)
  {
    rte_exit(-1, "create pktmbuf pool failure!\n");
  }
  
  int nb_ports = rte_eth_dev_count_avail();
    printf("--->DPDK-TESTER-DBG:%d ports load\n", nb_ports);
  
  if(nb_ports <= 0)
  {
    rte_exit(-1, "not detect any DPDK devices!\n");
  }

  int lcore_nb;
  lcore_nb = rte_lcore_count();
  printf("--->DPDK-TESTER-DBG:%d cores load\n", lcore_nb);
  
  /* parse cmdline */
  parse_params(argc - ret, argv + ret);

  printf("--->DPDK-TESTER-DBG:Cmd parse done!\n");

  rte_timer_subsystem_init();

  /* load ndn traces from file */
  ret = load_bench_trace(ginfo.trace_file, pms, NB_MAX_PM, ip_proto);

  if(ret <= 0){
      printf("Sender exit for no trace loaded...\n");
      exit(1);        
  }

  ginfo.total_trace = ret;//number of datas

  if(ret <= 0)
  {
    rte_exit(-1, "no invalid trace!\n");
  }
  
  uint32_t lcore_id = 0;
  uint32_t core_white_list = 0x0;

  RTE_LCORE_FOREACH_WORKER(lcore_id){
    core_white_list |= 1U << (lcore_id-1);
  }
  printf("Port Mask = 0x%x\n", core_white_list);

  int port_ids[MAX_CORES]   = {0};
  int is_tx_core[MAX_CORES] = {0};
  int is_fwd_core[MAX_CORES] = {0};

  configure_core_port_map(port_ids, is_tx_core, is_fwd_core, lcore_nb, nb_ports);

  int port_id;
  for(port_id = 0; port_id < nb_ports; ++port_id){
    one_port_setup(mbuf_pool, port_id, port_ids, is_tx_core, is_fwd_core);
  }

  /* show core config */
  RTE_LCORE_FOREACH_WORKER(lcore_id){
    printf("Core#%d, port_id=%d, rxq_id=%d, ", lcore_id, lc_args[lcore_id].port_id,
                          lc_args[lcore_id].rx.queue_id);
    if(is_tx_core[lcore_id] || is_fwd_core[lcore_id]){
      printf("txq_id=%d\n", lc_args[lcore_id].tx.queue_id);
    }
    else{
      printf("txq_id=None\n");
    }
  }
  
  /* final confirm */
  char start_cmd;
  printf("[Press any key but 'q' to start sender...]\n");
  start_cmd = getchar();
  if(start_cmd == 'q'){
      printf("Sender quit...\n");
      return 0;
  }
  else{
      printf("Sender ready to go...\n");
  }
  
  /* lunch task onto lcores and run */
  RTE_LCORE_FOREACH_WORKER(lcore_id)
  {
    if( ( ((uint32_t)1 << (lcore_id-1)) & core_white_list ) == 0  ){
        continue;              
    }
    rte_eal_remote_launch(worker_main, (void*)&lc_args[lcore_id], lcore_id);
  }

  print_stats(nb_ports);
  rte_eal_mp_wait_lcore();
  
  return 0;
}
