#ifndef TLOAD_H
#define TLOAD_H

#include <stdio.h>
#include "pm.h"

#define INVALID_LINE -1
#define END_LINE 0
#define VALID_LINE 1

#define NB_FIELD 8

#ifdef __cplusplus
extern "C"{
#endif

int load_bench_trace(const char *file, struct packet_model pms[], int max_traces, int ip_mode);

#ifdef __cplusplus
}
#endif

#endif
