#! /bin/bash

#t data file
#s speed Mbps
#L pkt length
TRACE_DIR=`pwd`"/data"
PKT_SIZE=128
TP_MAP=0x01
FWD_MAP=0x0
CORE_LIST=24-40
PREFIX="TX"
P0_PCIE=af:00.0
P1_PCIE=af:00.1

SPEED=$1
PROTO=$2
TRACE_FILE=$3

echo "Test Packt Size=$PKT_SIZE Bytes"

sudo ./build/dpdk-sender -l ${CORE_LIST} -n 4 -a ${P0_PCIE} -a ${P1_PCIE} --file-prefix ${PREFIX} -- -t ${TRACE_DIR}/${TRACE_FILE} -s ${SPEED} -L ${PKT_SIZE} -P ${TP_MAP} -F ${FWD_MAP} -p ${PROTO}
# sudo gdb --args ./build/dpdk-sender -l ${CORE_LIST} -n 4 -a ${P0_PCIE} -a ${P1_PCIE} --file-prefix ${PREFIX} -- -t ${TRACE_DIR}/${TRACE_FILE} -s ${SPEED} -L ${PKT_SIZE} -P ${TP_MAP} -F ${FWD_MAP}
